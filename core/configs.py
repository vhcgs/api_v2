from pydantic import BaseSettings
from sqlalchemy.ext.declarative import declarative_base


class Settings(BaseSettings):
    """
    Configurações gerais usadas na aplicação
    """
    API_V1_STR: str = '/api/v1'
   
    #credencias geradas pelo render colque a do banco que vocês criaram "postgresql+asyncpg://usuario do banco:senha@postgres.render.com:5432/nome da base de dados"
    DB_URL: str = "postgresql+asyncpg://ba_22203874_user:Y3SwcY940zyaAyaJkpWvnPA2JVuC6uuQ.com:5432/ba_22203874"
    DBBaseModel = declarative_base()

    class Config:
        case_sensitive = True


settings = Settings()
